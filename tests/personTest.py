#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import requests
import json


class PersonTest(unittest.TestCase):

    def setUp(self):
        self.url = "localhost:5000/api/person"
        self.headers = {'content-type': 'application/json'}

    def get_all(self):
        person = {"first_name": "potato"}
        data = json.dumps(person)

        r = requests.get(self.url, data=data)

        print(r.json)

        # self.assertEqual(r.json["first_name"], person["first_name"])
        # self.assertIsNotNone(r.json["id"])


if __name__ == '__main__':
    unittest.main()