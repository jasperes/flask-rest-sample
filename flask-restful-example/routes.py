#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.restless import APIManager
from .app import app, db
from .models import *


api_manager = APIManager(app, flask_sqlalchemy_db=db)
api_manager.create_api(Example, methods=['GET', 'POST', 'DELETE', 'PUT'])