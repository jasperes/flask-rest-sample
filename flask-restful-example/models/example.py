#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..app import db


class Example(db.Model):
    """ Example Class Model """

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.Text)
    last_name = db.Column(db.Text)