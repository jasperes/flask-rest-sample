Flask Restful Example
=============

Project example for a RESTFul webservice, with only CRUD system.
This project still in progress. See Develop branch for newer sources.

License
-------

GNU GENERAL PUBLIC LICENSE. This can be found at LICENSE file.

Dependencies
-------

The dependencies for this project are listed below:

* Flask
* Flask-SQLAlchemy
* Flask-Restless

All can be installed with:

    cd path/to/flask-restful-example
    pip install -r requirements

Configuring server
-------

All configurations for the webservice can be found in flask_restful_example/configs.

To set new configs, create another python file at this folder.

All options available can be found in default.py file.

When server is started, this will look to a environment variable named "FLASK_RESTFUL_EXAMPLE_CONFIG" for default.
To set config file, execute:

    export ENVIRONMENT_VARIABLE_NAME=configs/python_config_file.py

You can change the name of the variable in flask_restful_example/app.py:

    app.config.from_envvar('ENVIRONMENT_VARIABLE_NAME')

Starting Server
-------

The webservice can be started executing start.sh file.

You can change server mode changing config environment variable or creating a new .sh file, following this example.

Creating Models
-------

First, create a model in flask_restful_example/models/model_name.py
See the example in this folder. For more info how to configure, [read more here](http://docs.sqlalchemy.org/en/rel_0_9/orm/mapper_config.html)

After create a model, add a new line in flask_restful_example/models/__init__.py file:

    from .model_name import ModelClassName

Finally, add a new line in flask_restful_example/routes.py:

    api_manager.create_api(ModelClassName, methods=['GET', 'POST', 'DELETE', 'PUT'])

TODO
-------

This can be found at TODO file.
