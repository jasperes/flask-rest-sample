#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful_example.app import *


if __name__ == '__main__':
    print("creating data base...")
    db.create_all()
    print("Created!")
    print("Running...")
    app.run()